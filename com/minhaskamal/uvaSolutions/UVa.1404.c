/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 16.May.13
**/

#include <stdio.h>
#include <math.h>

int main ()
{
    int n, z;
    scanf("%d", &n);

    for(z=0; z<n; z++)
    {
        long long int a, b;
        scanf ("%lld %lld", &a, &b);
        int k, s;
        scanf("%d %d", &k, &s);
        if(a>b || k>9 || s>39) continue;
        long long int tup[k];

        int i;
        for(i=0; i<k-1; i++) tup[i]=-100;
        tup[k-1]=2;

        if(a%2==0) a=(a+1);  //odd making


        long long int x;
        long long int y, x2;
        char fl;
        int ans=0;

        for(x=a; x<=b; x=x+2)   //the program will work only with odd numbers
        {
            x2=sqrt(x);
            fl=1;

            for (y=3; y<=x2; y=y+2)  //verification loop
                if(x%y==0)
                {
                    fl=0;
                    break;
                }

            if (fl==1)  //printing
            {
                for(i=0; i<(k-1); i++) tup[i]=tup[i+1];
                tup[k-1]=x;

                if((tup[k-1]-tup[0])==s) ans++;
            }
        }

        printf("%d\n", ans);
    }


    return 0;
}



