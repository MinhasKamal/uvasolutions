/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 14.May.13
**/

#include <stdio.h>

int main()
{
    for(; ; )
    {
        int n, k;
        scanf("%d %d", &n, &k);
        if(n<=0 || k<=0) break;
        if(n>25 || k>40) continue;

        int s[n], i;
        for(i=0; i<n; i++) s[i]=0;

        int j=0, fns=0, ad=0;

        while(fns<=n)
        {
            if(n-fns==0) fns++;

            for(i=0; i<n; i++)
            {
                if(s[i]<0) continue;

                if(s[i]==40)
                {
                    s[i]=-1;
                    printf("%3d", i+1);

                    continue;
                }

                if(!ad)
                {
                    j++;

                    if((s[i]+j)>=40)
                    {
                        ad=s[i]+j-40;
                        s[i]=40;
                        fns++;
                    }
                    else s[i]=s[i]+j;
                }
                else
                {
                    if((s[i]+ad)>=40)
                    {
                        ad=s[i]+j-40;
                        s[i]=40;
                        fns++;
                    }
                    else
                    {
                        s[i]=s[i]+ad;
                        ad=0;
                    }
                }


                if(j==k) j=0;
            }
        }
        printf("\n");
    }

    return 0;
}

