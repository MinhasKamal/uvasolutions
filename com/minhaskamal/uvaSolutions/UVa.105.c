/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 20.May.13
**/

#include <stdio.h>

int main(int argc, char *argv[])
{
    if(argc!=2) exit(1);

    FILE *fp;

    if((fp=fopen(argv[1], "r"))==NULL) exit(1);

    int l[5001], h[5001], r[5001], i;
    for(i=0; i<5001; i++)
    {
        if(feof(fp)) break;
        fscanf(fp, "%d %d %d", &l[i], &h[i], &r[i]);
    }

    fclose(fp);

    int n=i;
    int L=l[0], R=r[0];
    for(i=0; i<n; i++)
    {
        if(l[i]<L) L=l[i];
        if(r[i]>R) R=r[i];
    }

    int j, H=0, H2;
    for(i=L; i<=R; i++)
    {
        H2=H;
        H=0;
        for(j=0; j<n; j++)
        {
            if(i>=l[j] && i<r[j])
                if(h[j]>H) H=h[j];
        }
        if(H2!=H) printf("%d %d ", i, H);
    }

    return 0;
}
