/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 16.May.13
**/

#include <stdio.h>

#define LETTER 500

int main()
{
    for(; ; )
    {
        char sen[LETTER];
        int w=0;
        gets(sen);

        int i=0, j=0, k=0;
        for(; sen[i]!='\0'; i=k)
        {
            j=k;
            for(; sen[j]!='\0'; j++)
                if((sen[j]>96 && sen[j]<123) || (sen[j]>64 && sen[j]<91)) break;

            k=j;
            for(; sen[k]!='\0'; k++)
                if(sen[k]==32)
                {
                    w++;
                    break;
                }
        }
    printf("%d\n", w+1);
    }
    return 0;
}
