/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 13.May.13
**/

#include <stdio.h>
#include <math.h>

int main()
{
    long int s, f;

    int cycle, i;
    scanf("%d", &cycle);
    for(i=0; i<cycle; i++)
    {
        scanf("%ld %ld", &s, &f);

        long int j, num=0;
        int a, fa, fac=0;

        for(j=s; j<=f; j++)
        {
            a=0;
            fa=0;

            long int y, z;
            z=sqrt(j);
            for(y=2; y<sqrt(j); y++)
            {
                if((j%y)==0) fa++;
            }

            if(j==z*z) a=1;
            fa=(2*fa)+2+a;

            if(fa>fac)
            {
                fac=fa;
                num=j;
            }
        }

        printf("Between %ld and %ld, %ld has a maximum of %d divisors.\n", s, f, num, fac);
    }


    return 0;
}
