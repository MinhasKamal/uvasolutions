/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 17.May.13
**/

#include <stdio.h>

int main(int argc, char *argv[])
{
    FILE *from, *to;
    if(argc!=3)
    {
        printf("File name is missing.\n");
        exit(1);
    }
    if((from=fopen(argv[1], "r"))==NULL)
    {
        printf("Error opening file.\n");
        exit(1);
    }
    if((to=fopen(argv[2], "w"))==NULL)
    {
        printf("Error writing file.\n");
        exit(1);
    }

    long long int a, b, x;
    long int m, n;
    int i, d;

    while(!feof(from))
    {
        a=0;
        b=1;
        fscanf(from, "%d ", &d);

        if(d%2==0 && d>0 && d<9)
        {
            for(i=1; i<=d; i++)
                a=a*10+9;
            for(i=1; i<=d/2; i++)
                b=b*10;

            for(x=0; x<=a; x++)
            {
                m=x%b;
                n=x/b;

                if(((m+n)*(m+n))==x)
                {
                    if(d==2) fprintf(to, "%.2d\n", x);
                    else if(d==4) fprintf(to, "%.4d\n", x);
                    else if(d==6) fprintf(to, "%.6d\n", x);
                    else fprintf(to, "%.8d\n", x);
                }
            }
        }

    }

    fclose(to);
    fclose(from);

    return 0;
}
