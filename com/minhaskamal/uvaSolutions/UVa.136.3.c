/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 12.May.13
**/

#include <stdio.h>
#include <time.h>

int main()
{
    unsigned long long int u=854296875, ug;
    int i=1498;

    for( ; i<1500; u++)
    {
        ug=u;

        if(u%2!=0 && u%3!=0 && u%5!=0) continue;

        if(u%7==0) continue;
        else if(u%11==0) continue;
        else if(u%13==0) continue;
        else if(u%17==0) continue;
        else if(u%19==0) continue;
        else if(u%23==0) continue;
        else if(u%29==0) continue;
        else if(u%31==0) continue;
        else if(u%37==0) continue;
        else if(u%41==0) continue;
        else if(u%43==0) continue;
        else if(u%47==0) continue;

        else if(u%53==0) continue;
        else if(u%59==0) continue;
        else if(u%61==0) continue;
        else if(u%67==0) continue;
        else if(u%71==0) continue;
        else if(u%73==0) continue;
        else if(u%79==0) continue;
        else if(u%83==0) continue;
        else if(u%89==0) continue;
        else if(u%91==0) continue;
        else if(u%97==0) continue;


        while(u%30==0) u=u/30;
        while(u%15==0) u=u/15;
        while(u%10==0) u=u/10;
        while(u%6==0) u=u/6;
        while(u%5==0) u=u/5;
        while(u%3==0) u=u/3;
        while(u%2==0) u=u/2;

        if(u==1) i++;
        u=ug;
    }
    printf("The 1500'th ugly number is %lld.", ug);

    return 0;
}

// 859963392

