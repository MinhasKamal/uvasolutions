/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 18.May.13
**/

#include <stdio.h>

void mo_ov(int a, int b, int *block);
void mo_on(int a, int b, int *block);
void pi_ov(int a, int b, int *block);
void pi_on(int a, int b, int *block);

int n;

int main()
{
    scanf("%d", &n);
    int block[n][n];

    int i, j;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            block[i][j]=-1;
    for(j=0; j<n; j++) block[j][0]=j;

    char c1[5], c2[5];
    int f, t;
    for(; ; )
    {
        scanf("%s ", c1);
        if(!strcmp(c1, "quit")) break;
        scanf("%d %s %d", &f, c2, &t);

        if(f==t || f<0 || f>n || t<0 ||t>n) continue;

        if(!strcmp(c1, "move"))
        {
            if(!strcmp(c2, "over")) mo_ov(f, t, block);
            else if(!strcmp(c2, "onto")) mo_on(f, t, block);
        }
        else if(!strcmp(c1, "pile"))
        {
            if(!strcmp(c2, "over")) pi_ov(f, t, block);
            else if(!strcmp(c2, "onto")) pi_on(f, t, block);
        }
    }

    for(i=0; i<n; i++)
    {
        printf(" %d:", i);
        for(j=0; j<n; j++)
            if(block[i][j]>=0) printf(" %d", block[i][j]);
        printf("\n");
    }

    return 0;
}

void mo_ov(int a, int b, int *block)
{
    int i, j;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            if(block[i][j]==a) break;
    int k,l;
    for(k=0; k<n; k++)
        for(l=0; l<n; l++)
            if(block[k][l]==b) break;

    if(i==k) return;

    int x;
    for(x=j; x<n-1; x++)
        block[i][x]=block[i][x+1];
    block[i][x]=-1;

    for(x=n-1; x>=0; x--)
        if(block[k][x]!=-1) break;
    block[k][x+1]=a;

    return;
}

void mo_on(int a, int b, int *block)
{
    int i, j;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            if(block[i][j]==a) break;
    int k,l;
    for(k=0; k<n; k++)
        for(l=0; l<n; l++)
            if(block[k][l]==b) break;

    if(i==k) return;

    int x;
    for(x=j; x<n-1; x++)
        block[i][x]=block[i][x+1];
    block[i][x]=-1;

    for(x=n-1; x>=0; x--)
    {
        if(block[k][x]==b) break;
        else block[k][x]=block[k][x-1];
    }
    block[k][x+1]=a;

    return;
}

void pi_ov(int a, int b, int *block)
{
    /*int i, j;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            if(block[i][j]==a) break;
    int k,l;
    for(k=0; k<n; k++)
        for(l=0; l<n; l++)
            if(block[k][l]==b) break;*/

    return;
}

void pi_on(int a, int b, int *block)
{
    /*int i, j;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            if(block[i][j]==a) break;
    int k,l;
    for(k=0; k<n; k++)
        for(l=0; l<n; l++)
            if(block[k][l]==b) break;*/

    return;
}
