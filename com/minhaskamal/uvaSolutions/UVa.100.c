/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 11.May.13
**/

#include <stdio.h>

int cycle(int i, int j);

int main()
{
    for( ; ; )
    {
        int i=0, j=0;
        scanf("%d %d", &i, &j);

        if(i<=0 || i>1000000 || j<=0 || j>1000000) break;

        printf("%d %d %d\n", i, j, cycle(i, j));
    }
    return 0;
}

int cycle(int i, int j)
{
    if(i>j)
    {
        i=i+j;
        j=i-j;
        i=i-j;
    }

    int  m=1;
    for( ; i<=j; i++)
    {
        int n=i, k;
        for(k=1; n>1; k++)
        {
            if(n%2==0) n=n/2;
            else n=3*n+1;
        }
        if(k>m) m=k;
    }
    return m;
}
