/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 18.May.13
**/

#include <stdio.h>
#include <string.h>

int main()
{
    int n;
    scanf("%d", &n);
    int block[n][n];

    int i, j;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            block[i][j]=-1;
    for(j=0; j<n; j++) block[j][0]=j;

    char c1[5], c2[5];
    int f, t;
    for(; ; )
    {
        scanf("%s", c1);
        if(!strcmp(c1, "quit")) break;
        scanf("%d%s%d", &f, c2, &t);

        if(f==t || f<0 || f>n || t<0 ||t>n) continue;

        if(!strcmp(c1, "move"))
        {
            if(!strcmp(c2, "over"))
            {
                int i2, j2;
                for(i2=0; i2<n; i2++)
                    for(j2=0; j2<n; j2++)
                        if(block[i2][j2]==f) break;
                int k, l;
                for(k=0; k<n; k++)
                    for(l=0; l<n; l++)
                        if(block[k][l]==t) break;

                //if(i2!=k)
                {
                    printf("3465\n");
                    int x;
                    for(x=j2; x<n-1; x++)
                        block[i2][x]=block[i2][x+1];
                    block[i2][x]=-1;

                    for(x=n-1; x>=0; x--)
                        if(block[k][x]>=0) break;
                    block[k][x+1]=f;
                }
            }
            else if(!strcmp(c2, "onto"))
            {
                int i2, j2;
                for(i2=0; i2<n; i2++)
                    for(j2=0; j2<n; j2++)
                        if(block[i2][j2]==f) break;
                int k, l;
                for(k=0; k<n; k++)
                    for(l=0; l<n; l++)
                        if(block[k][l]==t) break;

                //if(i2!=k)
                {
                    printf("3465\n");
                    int x;
                    for(x=j2; x<n-1; x++)
                        block[i2][x]=block[i2][x+1];
                    block[i2][x]=-1;

                    for(x=n-1; x>=0; x--)
                    {
                        if(block[k][x]==t) break;
                        else block[k][x]=block[k][x-1];
                    }
                    block[k][x+1]=f;
                }
            }
        }
        else if(!strcmp(c1, "pile"))
        {
            //if(!strcmp(c2, "over")) pi_ov(f, t, block);
            //else if(!strcmp(c2, "onto")) pi_on(f, t, block);
        }
    }

    for(i=0; i<n; i++)
    {
        printf(" %d:", i);
        for(j=0; j<n; j++)
            if(block[i][j]>=0) printf(" %d", block[i][j]);
        printf("\n");
    }

    return 0;
}
