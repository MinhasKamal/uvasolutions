/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 16.May.13
**/

#include <stdio.h>
#include <math.h>

int mod(int n, int a);

int main()
{
    for(; ; )
    {
        long int i;
        scanf("%d", &i);
        if(i<2) break;

        char flag=1;

        int j, i2=sqrt(i);
        char z=1; //prime number flag

        if(i%2==0) z=0;
        else
            for (j=3; j<=i2; j=j+2)
                if (i%j==0)
                {
                    z=0;
                    break;
                }

        if(z) flag=0;

        if(flag)
            for(j=2; j<i; j++)
                if(mod(i, j)!=j)
                {
                    flag=0;
                    break;
                }

        if(flag) printf("The number %d is a Carmichael number.\n", i);
        else printf("%d is normal.\n", i);
    }

    return 0;
}

int mod(int n, int a)
{
    int i, m;
    long int x=1;
    for(i=0; i<n; i++)
    {
        if(x>n) x=x%n;
        x=x*a;
    }

    m=x%n;
    return m;
}

