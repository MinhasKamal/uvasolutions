/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 13.May.13
**/

#include <stdio.h>
#include <math.h>

int main()
{
    int cycle, i;
    scanf("%d", &cycle);

    long int x[2][cycle];

    for(i=0; i<cycle; i++)
        scanf("%ld %ld", &x[0][i], &x[1][i]);


    for(i=0; i<cycle; i++)
    {
        long int j, num=0;
        int a, fa, fac=0;

        if(x[0][i]<1 || x[0][i]>x[1][i]) continue;

        for(j=x[0][i]; j<=x[1][i]; j++)
        {
            a=0;
            fa=0;

            long int y, z;
            z=sqrt(j);
            for(y=2; y<sqrt(j); y++)
            {
                if((j%y)==0) fa++;
            }

            if(j==z*z) a=1;
            fa=(2*fa)+2+a;

            if(fa>fac)
            {
                fac=fa;
                num=j;
            }
        }

        printf("Between %ld and %ld, %ld has a maximum of %d divisors.\n", x[0][i], x[1][i], num, fac);
    }

    return 0;
}

