/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 19.May.13
**/

#include <stdio.h>
#include <string.h>

int main()
{
    long int b[3], g[3], c[3];
    int i;
    for(; ; )
    {
        for(i=0; i<3; i++)
        {
            scanf("%ld", &b[i]);
            scanf("%ld", &g[i]);
            scanf("%ld", &c[i]);
        }

        int x=0;
        long int y=2147483646, y2=0;
        for(i=1; i<7; i++) //1-BCG, 2-BGC, 3-CBG, 4-CGB, 5-GBC, 6-GCB
        {
            if(i==1) y2=b[1]+b[2]+c[0]+c[2]+g[0]+g[1];
            else if(i==2) y2=b[1]+b[2]+c[0]+c[1]+g[0]+g[2];
            else if(i==3) y2=b[0]+b[2]+c[1]+c[2]+g[0]+g[1];
            else if(i==4) y2=b[0]+b[1]+c[1]+c[2]+g[0]+g[2];
            else if(i==5) y2=b[0]+b[2]+c[0]+c[1]+g[1]+g[2];
            else if(i==6) y2=b[0]+b[1]+c[0]+c[2]+g[1]+g[2];

            if(y2<y)
            {
                y=y2;
                x=i;
            }
        }

        char str[4];

        if(x==1) strcpy(str, "BCG");
        else if(x==2) strcpy(str, "BGC");
        else if(x==3) strcpy(str, "CBG");
        else if(x==4) strcpy(str, "CGB");
        else if(x==5) strcpy(str, "GBC");
        else if(x==6) strcpy(str, "GCB");


        printf("%s %ld\n", str, y);
    }

    return 0;
}
