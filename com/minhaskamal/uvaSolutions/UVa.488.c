/**
*Name: Minhas Kamal
*Occupation: Student (IIT,DU)
*Date: 16.May.13
**/

#include <stdio.h>

int main()
{
    int a, f;
    scanf("%d %d", &a, &f);

    int i, j, k;
    for(i=0; i<f; i++)
    {
        printf("\n");
        for(j=1; j<=a; j++)
        {
            printf("\n");
            for(k=0; k<j; k++)
                printf("%d", j);
        }
        for(j=a-1; j>0; j--)
        {
            printf("\n");
            for(k=0; k<j; k++)
                printf("%d", j);
        }
    }
    return 0;
}
