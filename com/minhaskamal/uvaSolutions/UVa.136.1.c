/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 12.May.13
**/

#include <stdio.h>
#include <time.h>

#define SERIAL 800

int main()
{
    clock_t start, finish;
    start = clock();
    unsigned long long int u=1, ug;
    int i=0;
    for( ; i<SERIAL; u++)
    {
        ug=u;

        if(ug>10) if(u%2!=0 && u%3!=0 && u%5!=0) continue;

        if(u%7==0) continue;
        else if(u%11==0) continue;
        else if(u%13==0) continue;
        else if(u%17==0) continue;
        else if(u%19==0) continue;
        else if(u%23==0) continue;
        else if(u%29==0) continue;
        else if(u%31==0) continue;
        else if(u%37==0) continue;
        else if(u%41==0) continue;
        else if(u%43==0) continue;
        else if(u%47==0) continue;

        if(ug>1000)
        {
            if(u%53==0) continue;
            else if(u%59==0) continue;
            else if(u%61==0) continue;
            else if(u%67==0) continue;
            else if(u%71==0) continue;
            else if(u%73==0) continue;
            else if(u%79==0) continue;
            else if(u%83==0) continue;
            else if(u%89==0) continue;
            else if(u%91==0) continue;
            else if(u%97==0) continue;
        }

        if(ug>100000)
        {
            if(u%101==0) continue;
            else if(u%103==0) continue;
            else if(u%107==0) continue;
            else if(u%109==0) continue;
            else if(u%113==0) continue;
            else if(u%127==0) continue;
            else if(u%131==0) continue;
            else if(u%137==0) continue;
            else if(u%139==0) continue;
            else if(u%149==0) continue;
        }

        if(ug>1000000)
        {
            if(u%151==0) continue;
            else if(u%157==0) continue;
            else if(u%163==0) continue;
            else if(u%167==0) continue;
            else if(u%173==0) continue;
            else if(u%179==0) continue;
            else if(u%181==0) continue;
            else if(u%191==0) continue;
            else if(u%193==0) continue;
            else if(u%197==0) continue;
            else if(u%199==0) continue;
        }

        if(ug>1000000)
        {
            while(u%960==0) u=u/960;
            while(u%600==0) u=u/600;
            while(u%480==0) u=u/480;
            while(u%240==0) u=u/240;
            while(u%150==0) u=u/150;
            while(u%120==0) u=u/120;
            while(u%100==0) u=u/100;
        }
            while(u%30==0) u=u/30;
            while(u%15==0) u=u/15;
            while(u%10==0) u=u/10;
            while(u%6==0) u=u/6;
            while(u%5==0) u=u/5;
            while(u%3==0) u=u/3;
            while(u%2==0) u=u/2;
            if(u==1) i++;
        u=ug;
    }
    printf("The %d'th ugly number is %lld.", SERIAL, ug);

    finish = clock();
     printf("\n\nTIME: %.3ld.%ld \n\n", (finish-start)/1000, (finish-start)%1000);

    return 0;
}

// 859963392 -> 318 sec
