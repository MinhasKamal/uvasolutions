/**
*Name: Minhas Kamal
*Occupation: Student (DU,IIT)
*Date: 12.May.13
**/

#include <stdio.h>
#include <time.h>
#include <math.h>

int main ()
{
    clock_t start, finish;
    start = clock();

    unsigned long long int ug=1, x;
    long long int y;
    int i=0;

    for( ; i<1500; ug++)
    {
        if(ug>10) if(ug%2!=0 && ug%3!=0 && ug%5!=0) continue;

        if(ug%7==0) continue;
        else if(ug%11==0) continue;
        else if(ug%13==0) continue;
        else if(ug%17==0) continue;
        else if(ug%19==0) continue;
        else if(ug%23==0) continue;
        else if(ug%29==0) continue;
        else if(ug%31==0) continue;
        else if(ug%37==0) continue;
        else if(ug%41==0) continue;
        else if(ug%43==0) continue;
        else if(ug%47==0) continue;


        char z, flag=1;

        for(x=51; x<=ug; x=x+2)
        {
            z=1;
            for (y=3; y<=sqrt(x); y=y+2)
            {
                if(x%y==0)
                {
                    z=0;
                    break;
                }
            }

            if(z==1)
            {
                flag=1;
                if(ug%x==0)
                {
                    flag=0;
                    break;
                }

            }
        }
        if(flag==1) i++;
     }
    printf("The ugly number is %lld.", ug-1);

    finish = clock();
    printf("\n\nTIME: %.3ld.%ld \n\n", (finish-start)/1000, (finish-start)%1000);

    return 0;
}



